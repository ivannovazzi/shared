const boom = require('@hapi/boom');

class CustomError extends Error {
  constructor(...args) {
    super(...args);
    Error.captureStackTrace(this, CustomError);

    const [errorType, message] = args;
    this.errorType = errorType;
    this.message = message;
    this.isServiceError = true;
  }
}

class ValidationError extends CustomError {
  constructor(type, message) {
    super(type, message);
  }
}

class NotFoundError extends CustomError {
  constructor(message) {
    super('not_found', message);
  }
}

class AuthenticationError extends CustomError {
  constructor(message) {
    super('not_authorized', message);
  }
}

class AuthorizationError extends CustomError {
  constructor(message) {
    super('forbidden', message);
  }
}

class InternalError extends CustomError {
  constructor(message) {
    super('internal', message);
  }
}

class NotAcceptableError extends CustomError {
  constructor(message) {
    super('not_acceptable', message);
  }
}

exports.ValidationError = ValidationError;
exports.NotFoundError = NotFoundError;
exports.AuthenticationError = AuthenticationError;
exports.AuthorizationError = AuthorizationError;
exports.InternalError = InternalError;
