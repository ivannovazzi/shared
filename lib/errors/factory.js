const boom = require('boom');
const {
  ValidationError,
  NotFoundError,
  AuthenticationError,
  AuthorizationError,
  InternalError,
} = require('./types');

function transformValidationError (error, ... messages) {
  const { isJoi, details } = error;
  if (isJoi) {
    return details.map((detail) => detail.message);
  }
  return error.concat(` `,  messages.join(' '));
};


const buildError = {
  unauthorized: (text) => new AuthenticationError(`${text}`),
  notFound: (type, id, payload = '') => new NotFoundError(`${type} ${id} was not found${payload ? ' ' + payload : ''}`),
  conflict: (type, id, text) => new ValidationError('conflict', `${type} ${id} ${text}`),
  notAcceptable: (type, id, text) => new NotAcceptableError(`${type} ${id} ${text}`),
  forbidden: (type, id, text) => new AuthorizationError(`${type} ${id} ${text}`),
  internal: (error) => new InternalError(error),
  badRequest: (error, ...messages) => new ValidationError('bad_request', transformValidationError(error, ...messages)),
};

function checkError (error) {
  if (error && error.isServiceError === true) {
    switch (error.errorType) {
      case 'not_authorized':
        return boom.unauthorized(error.message);
        break;
      case 'not_found':
        return boom.notFound(error.message);
        break;
      case 'conflict':
        return boom.conflict(error.message);
        break;
      case 'not_acceptable':
        return boom.notAcceptable(error.message);
        break;
      case 'forbidden':
        return boom.forbidden(error.message);
        break;
      case 'bad_request':
        return boom.badRequest(error.message);
        break;
      case 'internal':
        return boom.internal(error.message);
        break;
      default:
        return boom.internal('Unable to determine the cause');
    }
  }
  return error;
};

exports.checkError = checkError;
exports.buildError = buildError;
