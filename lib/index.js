const queue = require('./clients/rabbitmq');
const storage = require('./clients/storage');
const roles = require('./validators/roles');
const jwt = require('./middlewares/jwt');
const logger = require('./middlewares/logger');
const errorHandler = require('./middlewares/errorHandler');
const notFoundHandler = require('./middlewares/notFoundHandler');
const responseHandler = require('./middlewares/responseHandler');
const validationHandler = require('./middlewares/validationHandler');

exports.clients = {
  queue,
  storage,
};

exports.middlewares = {
  errorHandler,
  notFoundHandler,
  responseHandler,
  jwt,
  logger,
  validationHandler,
};

exports.validators = {
  roles,
}