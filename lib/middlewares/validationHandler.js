const Multer = require('multer');
const joi = require('@hapi/joi');
const { buildError } = require('../errors/factory');

exports.validateBody = function (validationSchema) {
  return function validateBody(req, res, next) {
    const { value, error } = validationSchema.validate(req.body);
    if (error) {
      next(buildError.badRequest(error));
    }
    next();
  };
};

exports.validateQuery = function (validationSchema) {
  return function validateQuery(req, res, next) {
    const { value, error } = validationSchema.validate(req.query);

    if (error) {
      next(buildError.badRequest(error));
    }
    next();
  };
};

exports.validateRoles = function (validation) {
  return function validateRoles(req, res, next) {
    const isValid = validation(req.user_roles);
    if (!isValid) {
      next(buildError.forbidden('This operation is forbidden'));
    }
    next();
  };
};

exports.validateFile = function(config) {
  const multer = Multer({
    storage: Multer.memoryStorage(),
    limits: {
      fileSize: config.fileSize || (1 * 1024 * 1024), // no larger than 1mb
    },
  });
  const validator = multer.single(config.field);

  return function validateFile(req, res, next) {
    const response = validator(req, res, function (error) {
      if (error) {
        next(buildError.badRequest(error.toString()));
      }
      next();
    });
  }
};