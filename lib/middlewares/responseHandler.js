const responseHandler = (req, res, next) => {
  if (res.locals.data !== undefined) {
    if (typeof res.locals.data === 'object') {
      res.json(res.locals.data);
    } else {
      res.send(res.locals.data);
    }
  } else {
    res.end();
  }
};

module.exports = responseHandler;
