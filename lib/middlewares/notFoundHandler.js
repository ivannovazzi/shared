const boom = require('@hapi/boom');

const { buildError } = require('../errors/factory');

const notFoundHandler = async (req, res, next) => {
  if (!req.route) {
    next(boom.notFound(`Route '${req.baseUrl}${req.url}' was not found`));
  }
  next();
};
module.exports = notFoundHandler;
