const winston = require('winston');
const expressWinston = require('express-winston');

const createWinstonLogger = () => {
  return expressWinston.logger({
    transports: [
      new winston.transports.Console({
        silent: false,
      }),
    ],
    meta: false,
    msg: 'HTTP {{req.method}} {{req.url}}',
    expressFormat: true,
    colorize: false,
    timestamp: true,
  });
};

module.exports = createWinstonLogger;
