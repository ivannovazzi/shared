const { checkError } = require('../errors/factory');

const errorHandler = (err, req, res, next) => {
  const parsedErr = checkError(err);
  const status = (parsedErr && parsedErr.output && parsedErr.output.statusCode) || 500;
  const message = (parsedErr && parsedErr.output && parsedErr.output.payload.message) || 'Internal Server Error';
  const error = parsedErr && parsedErr.output && parsedErr.output.payload.error;
  const stack = parsedErr && parsedErr.stack;

  if (status === 500) {
    console.log(stack);
  }
  res.status(status).json({
    status,
    message,
    error,
  });
};

module.exports = errorHandler;
