const jwt = require('jsonwebtoken');
const { buildError } = require('../errors/factory');


module.exports = function createAuthorize(secret) {
  return function authorize(req, res, next) {
    try {
      const [, token] = req.headers.authorization.split(' ');
      const decoded = jwt.verify(token, secret);

      if (!decoded.user.id) {
        next(buildError.unauthorized('Wrong token'));
      } else {
        // assing the user object to the request so that it can be used in the controllers
        req.refreshToken = req.headers['refresh-token'];
        req.jwt = token;
        req.user = decoded.user;
        req.user_id = decoded.user.id;
        req.user_roles = decoded.roles;
        req.isProvider = decoded.roles.includes('provider');
        req.isConsumer = decoded.roles.includes('consumer');
        req.isAdmin = decoded.roles.includes('admin');
        next();
      }
    } catch (err) {
      next(buildError.unauthorized('Not authenticated'));
    }
  };
};