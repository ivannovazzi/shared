exports.isProvider = (roles) => {
  return roles.includes('provider');
}

exports.isConsumer = (roles) => {
  return roles.includes('consumer');
}