const amqp = require('amqplib/callback_api');

class Queue {
  constructor(options = { host: 'localhost' }) {
    this.options = options;
    this.connection = undefined;
    this.channel = undefined;
  }

  async connect(retries = 0) {
    if (this.connection) {
      return;
    }
    return new Promise((resolve) => {
      amqp.connect(`amqp://${this.options.host}`, (connectionError, connection) => {
        if (connectionError) {
          console.log(connectionError)
          const retry = Math.min(20, retries + 1);
          console.error('Unable to connect to message queue, reconnecting in %s seconds', retry);
          setTimeout(() => {
            this.connect(retry);
          }, 1000 * retry);
        } else {
          console.log('Successfully connected to message queue')
          resolve(connection);
        }
      });
    });
  }

  async getChannel() {
    if (this.channel) {
      return this.channel;
    }
    if (!this.connection) {
      this.connection = await this.connect();
    }
    this.channel = await this.connection.createChannel();
    return this.channel;
  }

  async assertQueue(queue) {
    await this.getChannel();
    await this.channel.assertQueue(queue);
  }

  async send(queue, message) {
    await this.getChannel();
    await this.assertQueue(queue);

    this.channel.sendToQueue(queue, Buffer.from(JSON.stringify(message)));
  }

  async consume(queue, job) {
    await this.getChannel();
    await this.channel.assertQueue(queue);

    this.channel.consume(queue, async (msg) => {
      await job(JSON.parse(msg.content.toString('utf8')));
      this.channel.ack(msg);
    }, {
      noAck: false,
    });
  }
}

module.exports = Queue;