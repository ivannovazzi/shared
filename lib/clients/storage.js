const FileType = require('file-type');
const S3 = require('aws-sdk/clients/s3');

class Storage {
  constructor(config) {
    this.config = config;
    this.client = new S3({
      region: config.region,
      endpoint: config.endpoint,
      credentials: {
        accessKeyId: config.accessKeyId,
        secretAccessKey: config.secretAccessKey,
      },
    });
  }
  async upload(buffer, name, folder = '') {

    const { ext } = await FileType.fromBuffer(buffer);
    const fileName = `${folder}/${name}.${ext}`;
    const { bucket, endpoint } = this.config;

    await this.client.putObject({
      Body: buffer,
      Key: fileName,
      Bucket: bucket,
      ACL: 'public-read',
    }).promise();

    return `${endpoint}/${bucket}/${fileName}`;
  }
}

module.exports = Storage;
