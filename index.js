const { clients, middlewares, validators } = require('./lib');

module.exports = {
  clients,
  middlewares,
  validators,
};